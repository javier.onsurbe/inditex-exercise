package es.onsurbe.inditex.data;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.Date;
import java.util.List;

@Repository
public interface PriceRepository extends CrudRepository<Price, Integer> {
    @Query("SELECT p FROM Price p WHERE p.brandId=:brandId AND p.productId=:productId AND :date>=p.startDate AND :date<=p.endDate ORDER BY p.priority desc")
    List<Price> findPricesBy(Date date, Integer productId, Integer brandId);
}
