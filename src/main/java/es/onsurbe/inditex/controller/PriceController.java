package es.onsurbe.inditex.controller;

import es.onsurbe.inditex.data.Price;
import es.onsurbe.inditex.data.PriceRepository;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.Date;
import java.util.List;

@RestController
@RequestMapping("prices")
public class PriceController {
    private final PriceRepository priceRepository;

    public PriceController(PriceRepository priceRepository) {
        this.priceRepository = priceRepository;
    }

    @GetMapping("search")
    public ResponseEntity<Price> getProductPrice(@RequestParam("date") Date date, @RequestParam("productId") Integer productId, @RequestParam("brandId") Integer brandId) {
        List<Price> priceList = priceRepository.findPricesBy(date, productId, brandId);
        if (priceList.size() == 0) {
            return ResponseEntity.notFound().build();
        } else {
            return ResponseEntity.ok(priceList.get(0));
        }
    }
}
