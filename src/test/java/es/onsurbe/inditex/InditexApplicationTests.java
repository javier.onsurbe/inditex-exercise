package es.onsurbe.inditex;

import org.junit.jupiter.api.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@SpringBootTest
@AutoConfigureMockMvc
class InditexApplicationTests {

	Logger logger = LoggerFactory.getLogger(this.getClass());

	@Autowired
	private MockMvc mockMvc;

	@Test
	void testRequest1() throws Exception {

		String response = mockMvc.perform(
				get("/prices/search")
						.queryParam("date", "2020-06-14 10.00.00")
						.queryParam("productId", "35455")
						.queryParam("brandId", "1")
		)
				.andExpect(status().is(HttpStatus.OK.value()))
				.andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
				.andExpect(jsonPath("$.price").value(35.50))
				.andReturn().getResponse().getContentAsString();
		logger.info("Response={}", response);
	}

	@Test
	void testRequest2() throws Exception {

		String response = mockMvc.perform(
				get("/prices/search")
						.queryParam("date", "2020-06-14 16.00.00")
						.queryParam("productId", "35455")
						.queryParam("brandId", "1")
		)
				.andExpect(status().is(HttpStatus.OK.value()))
				.andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
				.andExpect(jsonPath("$.price").value(25.45))
				.andReturn().getResponse().getContentAsString();
		logger.info("Response={}", response);
	}

	@Test
	void testRequest3() throws Exception {

		String response = mockMvc.perform(
				get("/prices/search")
						.queryParam("date", "2020-06-14 21.00.00")
						.queryParam("productId", "35455")
						.queryParam("brandId", "1")
		)
				.andExpect(status().is(HttpStatus.OK.value()))
				.andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
				.andExpect(jsonPath("$.price").value(35.50))
				.andReturn().getResponse().getContentAsString();
		logger.info("Response={}", response);
	}

	@Test
	void testRequest4() throws Exception {

		String response = mockMvc.perform(
				get("/prices/search")
						.queryParam("date", "2020-06-15 10.00.00")
						.queryParam("productId", "35455")
						.queryParam("brandId", "1")
		)
				.andExpect(status().is(HttpStatus.OK.value()))
				.andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
				.andExpect(jsonPath("$.price").value(30.50))
				.andReturn().getResponse().getContentAsString();
		logger.info("Response={}", response);
	}

	@Test
	void testRequest5() throws Exception {

		String response = mockMvc.perform(
				get("/prices/search")
						.queryParam("date", "2020-06-16 21.00.00")
						.queryParam("productId", "35455")
						.queryParam("brandId", "1")
		)
				.andExpect(status().is(HttpStatus.OK.value()))
				.andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
				.andExpect(jsonPath("$.price").value(38.95))
				.andReturn().getResponse().getContentAsString();
		logger.info("Response={}", response);
	}

	@Test
	void testBrandNotFound() throws Exception {

		String response = mockMvc.perform(
				get("/prices/search")
						.queryParam("date", "2020-06-16 21.00.00")
						.queryParam("productId", "35455")
						.queryParam("brandId", "2")
		)
				.andExpect(status().is(HttpStatus.NOT_FOUND.value()))
				.andReturn().getResponse().getContentAsString();
		logger.info("Response={}", response);
	}

	@Test
	void testDateNotFound() throws Exception {

		String response = mockMvc.perform(
				get("/prices/search")
						.queryParam("date", "2019-06-16 21.00.00")
						.queryParam("productId", "35455")
						.queryParam("brandId", "1")
		)
				.andExpect(status().is(HttpStatus.NOT_FOUND.value()))
				.andReturn().getResponse().getContentAsString();
		logger.info("Response={}", response);
	}

}
